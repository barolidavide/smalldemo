from dolfin import *
# more logging as possible
import logging
logging.basicConfig(level=logging.DEBUG)
logging.getLogger('UFL').setLevel(logging.WARNING)
logging.getLogger('FFC').setLevel(logging.WARNING)

# We define an Interval Mesh from Dolfin 
mesh_interval =UnitIntervalMesh(10)
# We print out the ufl_cell and it is correct an "interval"
print(mesh_interval.ufl_cell())
#So one can define a FiniteElement 
element_line = FiniteElement("Lagrange",interval,1)
#Then a FunctionSpace
Gamma = FunctionSpace(mesh_interval,element_line)
# In the constructor of FunctionSpace UFL check in this way
assert(mesh_interval.ufl_cell() == element_line.cell())
# Now if we use MeshView to define our domain we "broke" this assert
#We construct a 2D domain
mesh = UnitSquareMesh(10,10)
# We mark the interface
interface_domain = CompiledSubDomain("x[0] == 0.5")
facet_func = EdgeFunction("size_t", mesh,0)
interface_domain.mark(facet_func,3)
# meshView construct this domain
submanifold_interface = MeshViewMapping.create_from_marker(facet_func, 3)

# Check type
assert(type(submanifold_interface.ufl_cell())==type(element_line.cell()))

# Check the name of cell 
assert(submanifold_interface.ufl_cell().cellname() == element_line.cell().cellname())
# Check as it is implemented in UFL
assert(submanifold_interface.ufl_cell() == element_line.cell())

#SubManifold_GammaSpace = FunctionSpace(submanifold_interface,element_line)

# To resolve the issue in a way that work for everycase 
# I modified the line 61 of UFL /functionSpace.py 
# element.cell().cellname() != domain_cell.cellname()
# This solve this issue and creates others...

