import dolfin
from dolfin import *
from ufl import *
import logging
logging.basicConfig(level=logging.DEBUG)
logging.getLogger('UFL').setLevel(logging.WARNING)
logging.getLogger('FFC').setLevel(logging.WARNING)

mesh = dolfin.UnitSquareMesh(10,10)
# Define SubDomain for right, left and interface domain
right_domain = dolfin.CompiledSubDomain("x[0] >= 0.5")

interface_domain = dolfin.CompiledSubDomain("x[0] == 0.5")

left_domain = dolfin.CompiledSubDomain("x[0] <= 0.5")
# Define the Cell, Edge Function for marking the mesh entities.
cell_func = dolfin.CellFunction("size_t", mesh,0)
facet_func = dolfin.EdgeFunction("size_t", mesh,0)
# Mark the cell/facet according with the SubDomain
left_domain.mark(cell_func,1)

right_domain.mark(cell_func,2)

interface_domain.mark(facet_func,3)

# Define the SubManifold using the static method of MeshViewMapping

submanifold_left = dolfin.MeshViewMapping.create_from_marker(cell_func, 1)

submanifold_right = dolfin.MeshViewMapping.create_from_marker(cell_func, 2)

submanifold_interface = dolfin.MeshViewMapping.create_from_marker(facet_func, 3)

element_triangle = dolfin.FiniteElement("Lagrange",triangle,1)

#Define the Function Space 
V1 = dolfin.FunctionSpace(submanifold_left,element_triangle)
V2 = dolfin.FunctionSpace(submanifold_right,element_triangle)

element_line = dolfin.FiniteElement("Lagrange",interval,1)
VGamma = dolfin.FunctionSpace(submanifold_interface,element_line)

Vg = dolfin.FunctionSpaceProduct(V1,VGamma)


(u1,u2)=dolfin.TrialFunction(Vg)
(v1,v2)=dolfin.TestFunction(Vg)

mass=u1*v2*dx(domain=submanifold_interface)


A=PETScMatrix()
assemble(mass,tensor=A)

