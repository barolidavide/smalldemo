import dolfin
from dolfin import *
from ufl import *
import logging
logging.basicConfig(level=logging.DEBUG)
logging.getLogger('UFL').setLevel(logging.WARNING)
logging.getLogger('FFC').setLevel(logging.WARNING)
parameters["ghost_mode"] = "shared_facet"

mesh = dolfin.UnitSquareMesh(10,10)
mesh.init(2,1)
mesh.init(1)
# Define SubDomain for right, left and interface domain
right_domain = dolfin.CompiledSubDomain("x[0] >= 0.5")

interface_domain = dolfin.CompiledSubDomain("x[0] == 0.5")

left_domain = dolfin.CompiledSubDomain("x[0] <= 0.5")
# Define the Cell, Edge Function for marking the mesh entities.
cell_func = dolfin.CellFunction("size_t", mesh,0)
facet_func = dolfin.EdgeFunction("size_t", mesh,0)
# Mark the cell/facet according with the SubDomain
left_domain.mark(cell_func,1)

right_domain.mark(cell_func,2)

interface_domain.mark(facet_func,3)

# Define the SubManifold using the static method of MeshViewMapping

submanifold_left = dolfin.MeshViewMapping.create_from_marker(cell_func, 1)

submanifold_right = dolfin.MeshViewMapping.create_from_marker(cell_func, 2)

submanifold_interface = dolfin.MeshViewMapping.create_from_marker(facet_func, 3)

cellInterface=[30,  31,  47,  63,  79,  95, 111, 127, 143, 159]
mc=EdgeFunction("double",mesh)
for cellidx in cellInterface:
       mc.array()[cellidx]=2.0


# MeshView Mapping from interface manifold
mappingInterface = submanifold_interface.topology().mapping
# Parent Mesh 
meshParent = mappingInterface.mesh()
# MeshView Mapping from left manifold
mappingLeft=submanifold_left.topology().mapping
cellIdx=mappingLeft.cell_map()
import numpy as np

cf_left= CellFunction("double",submanifold_left)

for edgeIdx in mappingInterface.cell_map():
    edge = Edge(meshParent, edgeIdx)
    
    # the number of two cell (parent index) of macro cells are
    for cellIdx in edge.entities(2):
        print(cellIdx)
        cell=Cell(meshParent,cellIdx)
        print(cell.owner())

    left_cellidx=edge.entities(2)[0]
    localIdx=np.where(np.in1d(cellIdx,left_cellidx))[0]
    cf_left.array()[localIdx]=1.0

    right_cellidx=edge.entities(2)[1]
    print(edgeIdx,edge.entities(2))

"""
file_lc=XDMFFile(mpi_comm_world(),"left_cellsOnGamma.xdmf")
file_lc.write(cf_left)
"""

element_triangle = dolfin.FiniteElement("Lagrange",triangle,1)

#Define the Function Space 
V1 = dolfin.FunctionSpace(submanifold_left,element_triangle)
V2 = dolfin.FunctionSpace(submanifold_right,element_triangle)

element_line = dolfin.FiniteElement("Lagrange",interval,1)
VGamma = dolfin.FunctionSpace(submanifold_interface,element_line)
#VGamma=dolfin.FunctionSpace(MeshView(submanifold_interface,2),element_line)

Vg = dolfin.FunctionSpaceProduct(V1,VGamma)
(u1,uGamma)=dolfin.TrialFunction(Vg)
(v1,vGamma)=dolfin.TestFunction(Vg)




"""
mappingMeshViewGamma=submanifold_interface.topology().mapping
cellInterface=mappingMeshViewGamma.cell_map()
mc=EdgeFunction("double",mesh,float(MPI.rank(mesh.mpi_comm())))
for cellidx in cellInterface:
    print(cellidx)
    if MPI.rank(mesh.mpi_comm())==0:
        mc.array()[cellidx]=2.0
    elif MPI.rank(mesh.mpi_comm())==1:
        mc.array()[cellidx]=4.0
"""
fileh=XDMFFile(mpi_comm_world(), "meshViewEdges_paral.xdmf")
fileh.write(mc)
print(submanifold_interface.topology().mapping.mesh().num_cells())
mappingMeshViewGamma=submanifold_interface.topology().mapping
cellInterface=mappingMeshViewGamma.cell_map()
print(cellInterface)


