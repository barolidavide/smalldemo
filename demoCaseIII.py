import dolfin
from dolfin import *
from ufl import *
import logging
logging.basicConfig(level=logging.DEBUG)
logging.getLogger('UFL').setLevel(logging.WARNING)
logging.getLogger('FFC').setLevel(logging.WARNING)
#parameters["ghost_mode"] = "shared_facet"

mesh = dolfin.UnitSquareMesh(10,10)
mesh.init_global(mesh.topology().dim())

#mesh.init(2,1)
#mesh.init(1)
# Define SubDomain for right, left and interface domain
right_domain = dolfin.CompiledSubDomain("x[0] >= 0.5")

interface_domain = dolfin.CompiledSubDomain("x[0] == 0.5")

left_domain = dolfin.CompiledSubDomain("x[0] <= 0.5")
# Define the Cell, Edge Function for marking the mesh entities.
cell_func = dolfin.CellFunction("size_t", mesh,0)
facet_func = dolfin.EdgeFunction("size_t", mesh,0)
# Mark the cell/facet according with the SubDomain
left_domain.mark(cell_func,1)

right_domain.mark(cell_func,2)

interface_domain.mark(facet_func,3)

# Define the SubManifold using the static method of MeshViewMapping

submanifold_left = dolfin.MeshViewMapping.create_from_marker(cell_func, 1)

submanifold_right = dolfin.MeshViewMapping.create_from_marker(cell_func, 2)

submanifold_interface = dolfin.MeshViewMapping.create_from_marker(facet_func, 3)


# MeshView Mapping from interface manifold
mappingInterface = submanifold_interface.topology().mapping

# Parent Mesh 
meshParent = mappingInterface.mesh()
# MeshView Mapping from left manifold
mappingLeft=submanifold_left.topology().mapping
mappingRight=submanifold_right.topology().mapping

cellIdxL=mappingLeft.cell_map()
print("Left Side  %s , processor %d"%(cellIdxL,MPI.rank(mpi_comm_world())))

print("Global index map mesh %s,processor %d " %(mesh.topology().global_indices(mesh.topology().dim()), MPI.rank(mpi_comm_world())))
print("Global index map subman left %s,processor %d " %(submanifold_left.topology().global_indices(mesh.topology().dim()), MPI.rank(mpi_comm_world())))


cellIdxR=mappingRight.cell_map()
print("Right Side %s, processor %d "%(cellIdxR,MPI.rank(mpi_comm_world())))

cellIdxG=mappingInterface.cell_map()
print("Gamma Side %s, processor %d "%(cellIdxG,MPI.rank(mpi_comm_world())))

cellIdxG=mappingInterface.cell_map()
print("Gamma Side %s, processor %d "%(cellIdxG,MPI.rank(mpi_comm_world())))


print("shared_entities  %s, processor %d"%(submanifold_left.topology().shared_entities(mesh.topology().dim()),MPI.rank(mpi_comm_world())))

#print("sharing process %d, processor %d"%(submanifold_left.sharing_processes(),MPI.rank(mpi_comm_word()) ))


#import numpy as np

#cf_left= dolfin.CellFunction("double",submanifold_left)


"""
#partitionInterface=dolfin.EdgeFunction("double",submanifold_interface,float(MPI.rank(submanifold_interface.mpi_comm())))
partitionLeft=dolfin.CellFunction("size_t",submanifold_left, MPI.rank(mpi_comm_world()))
partitionRight=dolfin.CellFunction("size_t",submanifold_right,MPI.rank(mpi_comm_world()))
#fileGamma=dolfin.XDMFFile(mpi_comm_world(),"output/gammaPart.xdmf")
fileLeft=dolfin.File("output/leftPart.pvd")
fileRight=dolfin.File("output/rightPart.pvd")
#fileGamma.write(partitionInterface)
fileLeft<< partitionLeft
fileRight<< partitionRight
"""

"""
for edgeIdx in mappingInterface.cell_map():
    edge = Edge(meshParent, edgeIdx)
    print(len(edge.entities(meshParent.topology().dim()))) 
"""
